package mx.edu.itsh.demolibs;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.sistemastpe.account_demo.StatementModuleConfig;
import com.sistemastpe.payment.PaymentModuleConfig;
import com.sistemastpe.prospect.ProspectModuleConfig;
import com.sistemastpe.wifi_support.WiFiSupportModuleConfig;


/**
 * Created by mahegots on 17/02/18.
 */

public class App extends MultiDexApplication {
    @Override
    public void onCreate() {
        super.onCreate();

        ProspectModuleConfig config = new ProspectModuleConfig(this);
        config.setup();

        PaymentModuleConfig config2 = new PaymentModuleConfig(this);
        config2.setup();

        StatementModuleConfig config3 = new StatementModuleConfig(this);
        config3.setup();

        WiFiSupportModuleConfig config4 = new WiFiSupportModuleConfig(this);
        config4.setup();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}
