package mx.edu.itsh.demolibs;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.sistemastpe.account_demo.view.activities.MyAcountActivity;
import com.sistemastpe.payment.view.activities.ServicesPaymentActivity;
import com.sistemastpe.prospect.view.activities.CoverageActivity;
import com.sistemastpe.tracing_demo.view.activities.ConfirmationCodeActivity;
import com.sistemastpe.wifi_support.view.activities.ConnectedDevicesActivity;
import com.sistemastpe.wifi_support.view.activities.OptimizeWifiActivity;
import com.sistemastpe.wifi_support.view.activities.StatusConnectionActivity;

public class MainActivity extends AppCompatActivity {

    private EditText getNoAccount;
    private Button setNoAccount;
    private  String noAccount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getNoAccount = (EditText) findViewById(R.id.main_get_no_account);
        setNoAccount = (Button) findViewById(R.id.main_set_no_account);


        findViewById(R.id.button_init_tracing).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(ConfirmationCodeActivity.launchWithAccount(MainActivity.this));
            }
        });
        findViewById(R.id.button_init_prospect).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(CoverageActivity.launch(MainActivity.this));
            }
        });

        findViewById(R.id.button_init).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(ServicesPaymentActivity.launchWithAccount(MainActivity.this, noAccount));
            }
        });

        findViewById(R.id.button_init_account).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(MyAcountActivity.launchWithAccount(MainActivity.this, noAccount));
            }
        });

        findViewById(R.id.button_init_devices).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(ConnectedDevicesActivity.launchWithAccount(MainActivity.this, noAccount));
            }
        });

        findViewById(R.id.button_init_optimized).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(OptimizeWifiActivity.launchWithAccount(MainActivity.this, noAccount));
            }
        });

       /* findViewById(R.id.button_alarms_wifi).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(StatusConnectionActivity.launchWithAccount(MainActivity.this, noAccount));
            }
        });*/

        setNoAccount.setOnClickListener(view -> {
            noAccount  = getNoAccount.getText().toString();
        });
    }
}
